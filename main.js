let list = document.querySelector(".list");
let widget = document.querySelector(".widget");

const weatherData = {
    tempUnit: 'C',
    windSpeedUnit: 'm/s',
    days: [
        { day: 'Monday', temp: 22, windDirection: 'north-east', windSpeed: 10, type: 'sunny' },
        { day: 'Tuesday', temp: 14, windDirection: 'north-west', windSpeed: 14, type: 'rainy' },
        { day: 'Wednesday', temp: 17, windDirection: 'south-east', windSpeed: 20, type: 'cloudy' },
        { day: 'Thursday', temp: 12, windDirection: 'north-east', windSpeed: 25, type: 'rainy' },
        { day: 'Friday', temp: 18, windDirection: 'south-east', windSpeed: 15, type: 'cloudy' },
        { day: 'Saturday', temp: 15, windDirection: 'south-west', windSpeed: 18, type: 'rainy' },
        { day: 'Sunday', temp: 25, windDirection: 'north-west', windSpeed: 30, type: 'sunny' },
    ]
}

weatherData.days.forEach(el => {
    list.innerHTML += `<li class="list-items"><span class="colored"><em>Day:</em> ${el.day} <em>Temperature:</em> ${el.temp} <sup>o</sup>${weatherData.tempUnit}</span></li>`;
})

let listItems = document.querySelectorAll(".list-items");
listItems.forEach((elem, i) => {

    let tempKelvin = weatherData.days[i].temp + 273.15;
    let speedKmh = weatherData.days[i].windSpeed * 3.6;

    // Weather widget
    elem.addEventListener("click", function () {
        widget.innerHTML = `<div class="widget-content"><p class="day">${weatherData.days[i].day}</p><p class="temp">${weatherData.days[i].temp} <sup>o</sup>${weatherData.tempUnit}</p><span class="windDirection"></span> <span class="windSpeed">${weatherData.days[i].windSpeed} ${weatherData.windSpeedUnit}</span><p class="type">${weatherData.days[i].type}</p></div>`;

        //Wind Direction
        let windDirection = document.querySelector(".windDirection");
        if (weatherData.days[i].windDirection == "north-east") {
            windDirection.innerHTML = '<i class="fas fa-arrow-right northeast"></i>';
        }
        else if (weatherData.days[i].windDirection == "north-west") {
            windDirection.innerHTML = '<i class="fas fa-arrow-right northwest"></i>';
        }
        else if (weatherData.days[i].windDirection == "south-east") {
            windDirection.innerHTML = '<i class="fas fa-arrow-right southeast"></i>';
        }
        else {
            windDirection.innerHTML = '<i class="fas fa-arrow-right southwest"></i>';
        }

        //Type
        let day = document.querySelector(".day");

        let iconSunny=document.createElement("i");
        iconSunny.setAttribute("class","fas fa-sun");
        iconSunny.style.cssText = "margin-left: 10px; font-size: 40px; color: yellow";

        let iconCloud=document.createElement("i");
        iconCloud.setAttribute("class","fas fa-cloud");
        iconCloud.style.cssText = "margin-left: 10px; font-size: 40px; color: #339AF0";

        let iconCloudRain=document.createElement("i");
        iconCloudRain.setAttribute("class","fas fa-cloud-rain");
        iconCloudRain.style.cssText = "margin-left: 10px; font-size: 40px; color: #339AF0";

        if (weatherData.days[i].type == "sunny") {
            day.appendChild(iconSunny);
        }
        else if (weatherData.days[i].type == "cloudy") {
            day.appendChild(iconCloud);
        }
        else{
            day.appendChild(iconCloudRain);
        }

        //Temp in Kelvin
        let temp = document.querySelector(".temp");
        temp.addEventListener("click", function () {
            if (temp.clicked) {
                temp.innerHTML = `${weatherData.days[i].temp} <sup>o</sup>${weatherData.tempUnit}`;
                temp.clicked = false;
            }
            else {
                temp.innerHTML = `${tempKelvin} K`;
                temp.clicked = true;
            }
        })

        //Speed in km/h
        let windSpeed = document.querySelector(".windSpeed");
        windSpeed.addEventListener("click", function () {
            if (windSpeed.clicked) {
                windSpeed.innerHTML = `${weatherData.days[i].windSpeed} ${weatherData.windSpeedUnit}`;
                windSpeed.clicked = false;
            }
            else {
                windSpeed.innerHTML = `${speedKmh} km/h`;
                windSpeed.clicked = true;
            }
        })
    })
})










